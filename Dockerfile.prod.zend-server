FROM janatzend/zend-server:9.1-php7.1-apache

#-------------------------------------------------------------------------------
# Zend Server Setup / Config
#-------------------------------------------------------------------------------

COPY ./etc/zs_config_prod.zip /zs_config.zip
COPY ./etc/shell_functions.rc /shell_functions.rc

RUN . /shell_functions.rc && \
    service zend-server start && \
    sleep 5 && \
    add_target && \
    config_import && \
    clean_libs && \
    restart_php && \
    service zend-server stop

COPY ./etc/apache.site.conf /etc/apache2/sites-available/000-default.conf

#-------------------------------------------------------------------------------
# App sources
#-------------------------------------------------------------------------------
COPY . /var/www/
RUN rm -f /var/www/html/index.html

#-------------------------------------------------------------------------------
# Labelling
#-------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
ARG DOCKER_IMAGE

LABEL com.roguewave.particulummobile.build-date=$BUILD_DATE \
      com.roguewave.particulummobile.name="$DOCKER_IMAGE" \
      com.roguewave.particulummobile.description="Particulum Mobile Visualizer App - needed for Docker API usage" \
      com.roguewave.particulummobile.vcs-ref=$VCS_REF \
      com.roguewave.particulummobile.vcs-url="$VCS_URL" \
      com.roguewave.particulummobile.version=$VERSION \
      com.roguewave.particulummobile.schema-version="1.0"
