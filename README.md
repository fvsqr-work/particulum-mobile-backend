# Partculum Mobile Backend #

Backend for Particulum Mobile based on Zend Expressive 2

## App
The Particulum Mobile application will generate dynamically requests to an API. White balls are popping up in the browser, if there is a collision between two of them they first become gray and a request is sent to the backend. The server is answering with a color code which has been calculated out of the hostname of the server. The ball which sent the request is changing the color accordingly to the responded color code. After a while the balls are disappearing and new ones are created randomly.

## Docker Builds
"Production" Docker images are being build either through the ```make``` command or via Codeship, Wercker and Codefresh. All of the systems are building slightly different Docker images based on the official PHP 7.0 php-fpm image. The redis extension is being compiled and the latest Zend Expressive sources including the dependencies downloaded by composer are baked in. Finally the images are being pushed with a specific tag (Name of the CI system) to the public Docker Hub. 

[![Codefresh build status]( https://g.codefresh.io/api/badges/build?repoOwner=janatzendteam&repoName=particulum-mobile-backend&branch=master&pipelineName=particulum-mobile-backend&accountName=janatzend&type=cf-1)]( https://g.codefresh.io/repositories/janatzendteam/particulum-mobile-backend/builds?filter=trigger:build;branch:master;service:5978957564f2b20001b2024c~particulum-mobile-backend)

[![](https://images.microbadger.com/badges/version/janatzend/particulum-mobile-backend:codeship.svg)](https://microbadger.com/images/janatzend/particulum-mobile-backend:codeship "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/janatzend/particulum-mobile-backend:codeship.svg)](https://microbadger.com/images/janatzend/particulum-mobile-backend:codeship "Get your own image badge on microbadger.com")
[ ![Codeship Status for janatzendteam/particulum-mobile-backend](https://app.codeship.com/projects/b9034fd0-00f0-0135-f29b-0aaf1b5f79e7/status?branch=master)](https://app.codeship.com/projects/212708)

[![](https://images.microbadger.com/badges/version/janatzend/particulum-mobile-backend:wercker.svg)](https://microbadger.com/images/janatzend/particulum-mobile-backend:wercker "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/janatzend/particulum-mobile-backend:wercker.svg)](https://microbadger.com/images/janatzend/particulum-mobile-backend:wercker "Get your own image badge on microbadger.com")
[![wercker status](https://app.wercker.com/status/922effd4096a8782db4e3bf64b008eaf/s/master "wercker status")](https://app.wercker.com/project/byKey/922effd4096a8782db4e3bf64b008eaf)

### Comparison
![](https://s3.eu-central-1.amazonaws.com/stuttgart-office-files/vault/video/particulum-mobile--codeship-docker-wercker.gif)

[Download mp4](https://s3.eu-central-1.amazonaws.com/stuttgart-office-files/vault/video/particulum-mobile--codeship-docker-wercker.mp4)