# Image and binary can be overidden with env vars.
DOCKER_IMAGE  ?= janatzend/particulum-mobile-backend
DOCKER_IMAGE2 ?= janatzend/particulum-mobile-backend-zendserver
DOCKER_IMAGE3 ?= janatzend/particulum-mobile-backend-nginx
DOCKERFILE    ?= Dockerfile
DOCKERFILE2   ?= Dockerfile.prod.zend-server
DOCKERFILE3   ?= Dockerfile.nginx

# Get the latest commit.
GIT_COMMIT = $(strip $(shell git rev-parse --short HEAD))

# Get the version number from the code
CODE_VERSION = $(strip $(shell git describe --tags --abbrev=0 2> /dev/null))

ifeq (x$(CODE_VERSION), x)
$(error You need to tag your code to build a release)
endif

# Find out if the working directory is clean
GIT_NOT_CLEAN_CHECK = $(shell git status --porcelain)
ifneq (x$(GIT_NOT_CLEAN_CHECK), x)
DOCKER_TAG_SUFFIX = "-dirty"
endif

# If we're releasing to Docker Hub, and we're going to mark it with the latest tag, it should exactly match a version release
ifeq ($(MAKECMDGOALS),release)
# Use the version number as the release tag.
DOCKER_TAG = $(CODE_VERSION)

# See what commit is tagged to match the version
VERSION_COMMIT = $(strip $(shell git rev-list $(CODE_VERSION) -n 1 | cut -c1-7))
ifneq ($(VERSION_COMMIT), $(GIT_COMMIT))
$(error You are trying to push a build based on commit $(GIT_COMMIT) but the tagged release version is $(VERSION_COMMIT))
endif

# Don't push to Docker Hub if this isn't a clean repo
ifneq (x$(GIT_NOT_CLEAN_CHECK), x)
$(error You are trying to release a build based on a dirty repo)
endif

else
# Add the commit ref for development builds. Mark as dirty if the working directory isn't clean
DOCKER_TAG = $(CODE_VERSION)-$(GIT_COMMIT)$(DOCKER_TAG_SUFFIX)
endif

# Build Docker image
build: docker_build output

# Build and push Docker image
release: docker_build docker_push output

docker_build:
	# Build Docker image
	docker build \
	--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
	--build-arg VERSION=$(CODE_VERSION) \
	--build-arg VCS_URL=`git config --get remote.origin.url` \
	--build-arg VCS_REF=$(GIT_COMMIT) \
	--build-arg DOCKER_IMAGE=$(DOCKER_IMAGE) \
	-t $(DOCKER_IMAGE):$(DOCKER_TAG) -f $(DOCKERFILE) .

	docker build \
	--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
	--build-arg VERSION=$(CODE_VERSION) \
	--build-arg VCS_URL=`git config --get remote.origin.url` \
	--build-arg VCS_REF=$(GIT_COMMIT) \
	--build-arg DOCKER_IMAGE=$(DOCKER_IMAGE2) \
	-t $(DOCKER_IMAGE2):$(DOCKER_TAG) -f $(DOCKERFILE2) .

	docker build \
	--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
	--build-arg VERSION=$(CODE_VERSION) \
	--build-arg VCS_URL=`git config --get remote.origin.url` \
	--build-arg VCS_REF=$(GIT_COMMIT) \
	--build-arg DOCKER_IMAGE=$(DOCKER_IMAGE3) \
	-t $(DOCKER_IMAGE3):$(DOCKER_TAG) -f $(DOCKERFILE3) .

docker_push:
	# Tag image as latest
	docker tag $(DOCKER_IMAGE):$(DOCKER_TAG) $(DOCKER_IMAGE):latest
	docker tag $(DOCKER_IMAGE3):$(DOCKER_TAG) $(DOCKER_IMAGE3):latest
	docker tag $(DOCKER_IMAGE2):$(DOCKER_TAG) $(DOCKER_IMAGE2):latest

	# Push to DockerHub
	docker push $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker push $(DOCKER_IMAGE):latest
	docker push $(DOCKER_IMAGE2):$(DOCKER_TAG)
	docker push $(DOCKER_IMAGE2):latest
	docker push $(DOCKER_IMAGE3):$(DOCKER_TAG)
	docker push $(DOCKER_IMAGE3):latest

output:
	@echo Docker Image:   $(DOCKER_IMAGE):$(DOCKER_TAG)
	@echo Docker Image 2: $(DOCKER_IMAGE2):$(DOCKER_TAG)
	@echo Docker Image 3: $(DOCKER_IMAGE3):$(DOCKER_TAG)
