<?php
namespace ParticulumMobile\CollisionCount;

use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\JsonResponse;

class RetrieveMiddleware implements MiddlewareInterface 
{
    private $storage;
    
    public function __construct(RetrieveInterface $storage) {
        $this->storage = $storage;    
    }
    
    /**
     * @return Response
     */
    public function process(Request $request, DelegateInterface $delegate)
    {
        $collisions = $this->storage->retrieve();
         
        return new JsonResponse(['collision-count' => $collisions]);
    }
}