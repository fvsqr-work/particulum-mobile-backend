<?php
namespace ParticulumMobile\CollisionCount;

interface IncreaseInterface {
    public function increase();
    public function getHeaderName();
}