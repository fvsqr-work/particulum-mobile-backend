<?php
namespace ParticulumMobile\CollisionCount;

class Session implements IncreaseInterface, RetrieveInterface {
    
    const HEADER_NAME = 'X-Collision-Count-Session';
    
    public function increase() {
        $collisions = $this->retrieve();
        $_SESSION['collision-count'] = ++$collisions;
        
        return $collisions;
    }
    
    public function retrieve() {
        return $_SESSION['collision-count'] ?? 0;
    }
    
    public function getHeaderName() {
        return self::HEADER_NAME;
    }
}