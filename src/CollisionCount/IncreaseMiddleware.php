<?php
namespace ParticulumMobile\CollisionCount;

use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class IncreaseMiddleware implements MiddlewareInterface 
{
    private $storage;
    
    public function __construct(IncreaseInterface $storage) {
        $this->storage = $storage;    
    }
    
    /**
     * @return Response
     */
    public function process(Request $request, DelegateInterface $delegate)
    {
        $collisions = $this->storage->increase();
         
        $response = $delegate->process($request);
        
        $headerName = $this->storage->getHeaderName();
        
        $response = $response->withAddedHeader('Access-Control-Expose-Headers', $headerName);
        return $response->withHeader($headerName, $collisions);
    }
}