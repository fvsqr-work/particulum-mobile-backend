<?php
namespace ParticulumMobile\CollisionCount;

class Redis implements IncreaseInterface, RetrieveInterface {
    
    const HEADER_NAME = 'X-Collision-Count-Redis';
    
    const COLLISION_COUNT_KEY = 'collision-count';
    
    private $redis;
    
    public function __construct(\Redis $redis) {
        $this->redis = $redis;
    }
    
    public function increase() {
        $collisions = $this->retrieve();
         
        $this->redis->set(self::COLLISION_COUNT_KEY, ++$collisions);
        
        return $collisions;
    }
    
    public function retrieve() {
        return $this->redis->get(self::COLLISION_COUNT_KEY) ?: 0;
    }

    public function getHeaderName()
    {
        return self::HEADER_NAME;        
    }

    
    
}