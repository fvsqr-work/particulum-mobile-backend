<?php
namespace ParticulumMobile\CollisionCount;

interface RetrieveInterface {
    public function retrieve();
}