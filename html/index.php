<?php
namespace ParticulumMobile;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Zend\Expressive\AppFactory;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequest;

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

$app = AppFactory::create(require 'config/container.php');

$app->pipe('/', function ($request, DelegateInterface $delegate) : Response {
    session_start();

    $response = $delegate->process($request);

    $response = $response->withAddedHeader('Access-Control-Expose-Headers', 'X-SessionId');
    return $response->withHeader('X-SessionId', session_id());
});

$app->pipe('/', new \Tuupola\Middleware\Cors([
    "origin" => ["*"],
    "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
    "headers.allow" => [],
    "headers.expose" => [],
    "credentials" => true,
    "cache" => 0,
]));

$app->pipeRoutingMiddleware();

$app->pipe('/particle-color', 'IncreaseCollisionCountWithRedis');
$app->pipe('/particle-color', 'IncreaseCollisionCountWithSession');

$app->pipeDispatchMiddleware();

$app->get('/collision-count', 'RetrieveCollisionCountFromRedis');
$app->get('/collision-count-per-session', 'RetrieveCollisionCountFromSession');

$app->get('/particle-color', function ($request, DelegateInterface $delegate) {
    $res = [
        'hostname' => gethostname(),
        'ip' => gethostbyname(gethostname()),
        'date' => date('Y-m-d H:i:s'),
        'color' => dechex(intval(gethostname(), 16) % 0xffffff)
    ];

    return new JsonResponse($res);
});

$app->post('/particle-color-by-container-id', function (ServerRequest $request, DelegateInterface $delegate) {
    $containerId = $request->getParsedBody()['container_id'];

    $res = [
        'date' => date('Y-m-d H:i:s'),
        'color' => dechex(intval($containerId, 16) % 0xffffff),
        'containerId' => $containerId
    ];

    return new JsonResponse($res);
});

$app->run();
