<?php
/**
 * @license http://opensource.org/licenses/BSD-2-Clause BSD-2-Clause
* @copyright Copyright (c) Matthew Weier O'Phinney
*/
namespace ParticulumMobile;

use ParticulumMobile\CollisionCount\Redis;
use ParticulumMobile\CollisionCount\Session;
use ParticulumMobile\CollisionCount\IncreaseMiddleware;
use ParticulumMobile\CollisionCount\RetrieveMiddleware;

return ['dependencies' => [
    'aliases' => [
    ],
    'factories' => [
        'IncreaseCollisionCountWithRedis' => function ($sm) {
            $storage = $sm->get(Redis::class);
            return new IncreaseMiddleware($storage);
        },
        'IncreaseCollisionCountWithSession' => function ($sm) {
            $storage = $sm->get(Session::class);
            return new IncreaseMiddleware($storage);
        },
        'RetrieveCollisionCountFromRedis' => function ($sm) {
            $storage = $sm->get(Redis::class);
            return new RetrieveMiddleware($storage);
        },
        'RetrieveCollisionCountFromSession' => function ($sm) {
            $storage = $sm->get(Session::class);
            return new RetrieveMiddleware($storage);
        },
        Redis::class => function () {
            $redis = new \Redis();
            $redis->connect('redis');
            
            return new Redis($redis);
        }
    ],
    'invokables' => [
        Session::class => Session::class     
    ]
]];