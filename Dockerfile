FROM php:7.0-fpm

RUN apt-get update && apt-get install -y git libcurl4-gnutls-dev zlib1g-dev libicu-dev g++ libxml2-dev libpq-dev \
 && git clone -b php7 https://github.com/phpredis/phpredis.git /usr/src/php/ext/redis \
 && docker-php-ext-install redis \
 && apt-get autoremove && apt-get autoclean \
 && rm -rf /var/lib/apt/lists/*

#-------------------------------------------------------------------------------
# App sources
#-------------------------------------------------------------------------------
COPY . /app/

#-------------------------------------------------------------------------------
# Labelling
#-------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
ARG DOCKER_IMAGE

LABEL com.roguewave.particulummobile.build-date=$BUILD_DATE \
      com.roguewave.particulummobile.name="$DOCKER_IMAGE" \
      com.roguewave.particulummobile.description="Particulum Mobile Backend App - based on Zend Expressive" \
      com.roguewave.particulummobile.vcs-ref=$VCS_REF \
      com.roguewave.particulummobile.vcs-url="$VCS_URL" \
      com.roguewave.particulummobile.version=$VERSION \
      com.roguewave.particulummobile.schema-version="1.0"
