# codeship commands

## encrypt docker credentials
In order to encrypt, the aes file for the project has to be downloaded from the
Codeship project settings and placed into the project dir. Maybe I'm using it wrong,
but for me it only worked when renaming this file to codeship.aes . Though, haven't
tried the ```--key-path``` flag of ```jet encrypt```.

On MacOS: please uncheck first the option for secret storage in keychain in preferences of Docker

```
jet encrypt ${HOME}/.docker/config.json  dockercfg.json.encrypted
```
